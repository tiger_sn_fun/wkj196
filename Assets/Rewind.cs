using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rewind : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    float speed = 5f;
    //adjust this to change how high it goes
    float height = 0.25f;

    void Update()
    {
        //get the objects current position and put it in a variable so we can access it later with less code
        //Vector3 pos = gameObject.transform.position;
        //calculate what the new Y position will be
        //float newY = Mathf.Sin(Time.time * speed);
        //set the object's Y to the new calculated Y
        //transform.position = new Vector3(pos.x, newY * height, pos.z);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<CharacterController2D>().getRewind();
            gameObject.active = false;
        }
    }
}
