using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeatherController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void resetFeathers()
    {
        foreach (Transform feather in gameObject.GetComponentInChildren<Transform>())
        {
            feather.gameObject.SetActive(true);
        }
    }
}
