using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public CharacterController2D controller;
	Rigidbody2D rb;

	float horizontalMove = 0f;
	public float runSpeed = 40f;


	bool jump = false;
	bool crouch = false;
	bool dash = false;
	public static bool canMove = true;

    private void Start()
    {
		rb = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void Update () {
		if (canMove)
		{
			horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
			Debug.Log(horizontalMove);
			if (Input.GetButtonDown("Jump"))
			{
				jump = true;
			}
			if (Input.GetKeyDown("x"))
			{
				if (!controller.getDashed())
				{
					dash = true;
					rb.gravityScale = .5f;
					controller.dash();
					StartCoroutine(waitDashCooldown());
				}
			}
		}
	}

	void FixedUpdate ()
	{
		controller.Move(horizontalMove * Time.fixedDeltaTime, jump);
		if (!dash)
        {
			rb.velocity = new Vector2(horizontalMove * Time.fixedDeltaTime * runSpeed, rb.velocity.y);
		}
		jump = false;
		
	}

	IEnumerator waitDashCooldown()
    {
		yield return new WaitForSeconds(0.1f);
		dash = false;
		rb.gravityScale = 3;
    }
}
