using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBody : MonoBehaviour
{
    private bool isRewinding = false;
    public CharacterController2D player;
    public Rigidbody2D rb;

    List<Vector3> positions;

    // Start is called before the first frame update
    void Start()
    {
        positions = new List<Vector3>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("z"))
            StartRewind();
        if (Input.GetKeyUp("z"))
            StopRewind();
    }

    private void FixedUpdate()
    {
        if (isRewinding)
            Rewind();
        else
            Record();
    }

    void Record()
    {
        if (positions.Count == 0)
        {
            positions.Insert(0, transform.position);
            print("Record 1");
        } else
        {
            if (positions[0] != transform.position)
            {
                positions.Insert(0, transform.position);
                print("Record 2");
            }
        }
    }

    void Rewind()
    {
        if (player.hasRewindPower())
        {
            if (positions.Count > 0)
            {
                rb.bodyType = RigidbodyType2D.Kinematic;
                transform.position = positions[0];
                positions.RemoveAt(0);
            }
            else
            {
                rb.bodyType = RigidbodyType2D.Dynamic;
            }
        }
    }

    public void StartRewind()
    {
        isRewinding = true;
    }

    public void StopRewind()
    {
        isRewinding = false;
        rb.bodyType = RigidbodyType2D.Dynamic;
    }
}
