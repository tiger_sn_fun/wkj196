using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NPC : MonoBehaviour
{

    bool isPlayerClose = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F))
        {
            OnPlayerPressed();
        }
    }

    [SerializeField]
    UnityEvent m_PlayerEntered = default;
    public event UnityAction PlayerEntered
    {
        add { m_PlayerEntered.AddListener(value); }
        remove { m_PlayerEntered.RemoveListener(value); }
    }
    void OnPlayerEntered()
    {
        isPlayerClose = true;
        m_PlayerEntered.Invoke();
    }

    [SerializeField]
    UnityEvent m_OnPlayerLeft = default;
    public event UnityAction OnPlayerLeft
    {
        add { m_OnPlayerLeft.AddListener(value); }
        remove { m_OnPlayerLeft.RemoveListener(value); }
    }
    void OnOnPlayerLeft()
    {
        isPlayerClose = false;
        m_OnPlayerLeft.Invoke();
    }

    [SerializeField]
    UnityEvent m_PlayerPressed = default;
    public event UnityAction PlayerPressed
    {
        add { m_PlayerPressed.AddListener(value); }
        remove { m_PlayerPressed.RemoveListener(value); }
    }
    void OnPlayerPressed()
    {
        m_PlayerPressed.Invoke();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            OnPlayerEntered();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            OnOnPlayerLeft();
        }
    }
}
